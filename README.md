# ZXAD implementation docker image

ZXAD (pronounced "zed-zad") is a zero-knowledge based _private_ Tor
exit abuse detection system.  ZXAD detects large-volume traffic attacks
without revealing any information, apart from the fact that some user is
conveying a high volume of traffic through Tor.

See [our paper](https://cs.uwaterloo.ca/~iang/pubs/zxad-wpes21.pdf)
for more information:

Akshaya Mani and Ian Goldberg. "ZXAD: Zero-knowledge Exit Abuse
Detection for Tor".  20th ACM Workshop on Privacy in the Electronic
Society, November 2021.

This repository contains a docker image with the source code of a
kyber-based implementation (in go) and a libsnark-based implementation
(in C++), as well as timing scripts.

## Creating the docker image

You can either build the docker image yourself with `./build-docker`,
_or_ download and install the image with `./download-docker` (note:
718MB download!).  Either method will create a docker image named
`zxad`.

## Running the docker image

Run the image in the usual way: `docker run -it zxad bash`

## Running the libsnark-based experiments

The libsnark-based code is in the `snarkcode` directory:

  * `cd snarkcode`
  * `./run_timings`
  * Wait a while for it to finish.  It will write its output to the file `zxad_mnt4_true.out`
  * `./get_timings < zxad_mnt4_true.out > zxad_timings.csv` will output
    a csv with the measured timings.
    
    The first few columns are:
    * mnt4 or mnt6: which curve the zkSNARK is over (defaults to mnt4)
    * true or false: whether the G2 group cofactor is cleared inside the
      zkSNARK itself (defaults to true)
    * the maximum number of unlinkable connections to a given webserver
      allowed

    The remaining columns are the name of each step of the proof or
    verification, and the time for that step in milliseconds.

## Running the kyber-based experiments

The kyber-based code is in the `code` directory:

* `cd code/src`

The three benchmarks can be run by:

1. Blind signature transfer and BLS signature

      * `cd Bproof/benchmark/`
      * `./benchmark.sh [no_of_iterations]` will write output to benchmark.csv
      * Run without argument for default value (no_of_iterations = 2500) used in the paper.
      * The columns in benchmark.csv are the iteration number, the BLS timing, the proving time, the verifying time, and the proof size.

2. Circuit token proof
      * `cd Circuitproof/benchmark/`
      * `./benchmark.sh [no_of_iterations]` will write output to benchmark.csv
      * Run without argument for default value (no_of_iterations = 2500) used in the paper.
      * The columns in benchmark.csv are the iteration number, the proving time, the verifying time, and the proof size.

3. Stream token proof and Hash to G1
      * `cd Streamproof/benchmark/`
      * `./benchmark.sh [no_of_iterations] [ring_length]` will write output to benchmark.csv
      * Run without arguments for default value (no_of_iterations = 100 and ring_length = 25) used in the paper.
      * The columns in benchmark.csv are the ring size, the iteration number, the hash to G1 timing, the proving time, the verifying time, and the proof size.

The proving and verifying times for the stream token proof are in milliseconds. All other timings are in microseconds. All proof sizes are in bytes.
